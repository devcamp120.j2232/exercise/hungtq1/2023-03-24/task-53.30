import s10.Employee;
public class Task5330 {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "Linh", "Lai", 10000000);
        Employee employee2 = new Employee(2, "Minh", "Nguyen", 12000000);
        //in 2 đối tượng
        System.out.println("Task 3 - nhan vien 1: " + employee1);
        System.out.println("Task 3 - nhan vien 2: " + employee2);
        //in tên đầy đủ và lương năm
        System.out.println("Task 4 - nhan vien 1 ten la: " + employee1.getName());
        System.out.println("Task 4 - nhan vien 1 luong nam: " + employee1.getAnnualSalary());
        System.out.println("Task 4 - nhan vien 2 ten la: " + employee2.getName());
        System.out.println("Task 4 - nhan vien 2 luong nam: " + employee2.getAnnualSalary());
        //tăng lương 2 nhân viên
        System.out.println("Task 4 - nhan vien 1 tang luong 10%, luong moi la: " + employee1.raiseSalary(10));
        System.out.println("Task 4 - nhan vien 1 tang luong 5%, luong moi la: " + employee2.raiseSalary(5));
    }
}
